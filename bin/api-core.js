require("source-map-support").install();
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _express = __webpack_require__(1);
	
	var _express2 = _interopRequireDefault(_express);
	
	var _cookieParser = __webpack_require__(2);
	
	var _cookieParser2 = _interopRequireDefault(_cookieParser);
	
	var _bodyParser = __webpack_require__(3);
	
	var _bodyParser2 = _interopRequireDefault(_bodyParser);
	
	var _passport = __webpack_require__(4);
	
	var _passport2 = _interopRequireDefault(_passport);
	
	var _middlewareResponseJs = __webpack_require__(5);
	
	var _middlewareResponseJs2 = _interopRequireDefault(_middlewareResponseJs);
	
	var _middlewareNotFoundJs = __webpack_require__(6);
	
	var _middlewareNotFoundJs2 = _interopRequireDefault(_middlewareNotFoundJs);
	
	var _serviceConnectionManagerJs = __webpack_require__(10);
	
	var _serviceConnectionManagerJs2 = _interopRequireDefault(_serviceConnectionManagerJs);
	
	var _helperUrlHelperJs = __webpack_require__(16);
	
	var _helperUrlHelperJs2 = _interopRequireDefault(_helperUrlHelperJs);
	
	var _http = __webpack_require__(17);
	
	var _socketIo = __webpack_require__(18);
	
	var _socketIo2 = _interopRequireDefault(_socketIo);
	
	var _middlewareServerErrorJs = __webpack_require__(19);
	
	var _middlewareServerErrorJs2 = _interopRequireDefault(_middlewareServerErrorJs);
	
	var _config = __webpack_require__(12);
	
	var _config2 = _interopRequireDefault(_config);
	
	var _storeRolesStoreJs = __webpack_require__(20);
	
	var _storeRolesStoreJs2 = _interopRequireDefault(_storeRolesStoreJs);
	
	var _serviceTokenServiceJs = __webpack_require__(21);
	
	var _serviceTokenServiceJs2 = _interopRequireDefault(_serviceTokenServiceJs);
	
	var _serviceEntityManagerJs = __webpack_require__(24);
	
	var _serviceEntityManagerJs2 = _interopRequireDefault(_serviceEntityManagerJs);
	
	var _serviceBootstrapJs = __webpack_require__(25);
	
	var _serviceBootstrapJs2 = _interopRequireDefault(_serviceBootstrapJs);
	
	var app = (0, _express2['default'])();
	var server = (0, _http.Server)(app);
	var io = (0, _socketIo2['default'])(server);
	
	app.use(_middlewareResponseJs2['default']);
	
	app.use(_bodyParser2['default'].json());
	app.use(_bodyParser2['default'].urlencoded({ extended: false }));
	app.use((0, _cookieParser2['default'])());
	app.use(__webpack_require__(49)());
	app.use(__webpack_require__(48)({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));
	app.use(_passport2['default'].initialize());
	app.use(_passport2['default'].session());
	
	var project = process.env.project || _config2['default'].application.project,
	    user = process.env.user || _config2['default'].application.user,
	    port = process.env.port || _config2['default'].application.port,
	    prefix = '/' + user + '/' + project;
	
	io.of(prefix).on('connection', function (socket) {});
	
	_serviceBootstrapJs2['default'].init({ project: project, user: user, port: port }, app).then(function (router) {
	    app.use(router);
	    _serviceBootstrapJs2['default'].systemApi({ project: project, user: user, port: port });
	    app.use(_middlewareNotFoundJs2['default']);
	
	    app.use(_middlewareServerErrorJs2['default']);
	
	    console.log(JSON.stringify({ success: true, prefix: prefix, message: 'Application ' + project + ' for ' + user + ' started', metadata: process.env, models: Object.keys(_serviceEntityManagerJs2['default'].hash) }));
	    server.listen(port || 3000);
	});

/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = require("express");

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = require("cookie-parser");

/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = require("body-parser");

/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports = require("passport");

/***/ },
/* 5 */
/***/ function(module, exports) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	exports["default"] = function (req, res, next) {
	    res.response = function (response) {
	        response.response = res;
	        response.send();
	    };
	
	    next();
	};
	
	module.exports = exports["default"];

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _responseErrorResponseJs = __webpack_require__(7);
	
	var _responseErrorResponseJs2 = _interopRequireDefault(_responseErrorResponseJs);
	
	exports['default'] = function (req, res, next) {
	    var err = new Error('Not Found');
	    err.status = 404;
	    res.response(new _responseErrorResponseJs2['default']("Not Found", {}, false, 404));
	};
	
	module.exports = exports['default'];

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var _JsonResponseJs = __webpack_require__(8);
	
	var _JsonResponseJs2 = _interopRequireDefault(_JsonResponseJs);
	
	var ErrorResponse = (function (_JsonResponse) {
	    _inherits(ErrorResponse, _JsonResponse);
	
	    function ErrorResponse(message, data, success, status) {
	        _classCallCheck(this, ErrorResponse);
	
	        _get(Object.getPrototypeOf(ErrorResponse.prototype), 'constructor', this).call(this, data, success, status);
	
	        this.message = message;
	    }
	
	    _createClass(ErrorResponse, [{
	        key: 'data',
	        get: function get() {
	            return {
	                message: this.message,
	                data: this._data,
	                success: this._success
	            };
	        }
	    }]);
	
	    return ErrorResponse;
	})(_JsonResponseJs2['default']);
	
	exports['default'] = ErrorResponse;
	module.exports = exports['default'];

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var _ResponseJs = __webpack_require__(9);
	
	var _ResponseJs2 = _interopRequireDefault(_ResponseJs);
	
	var JsonResponse = (function (_Response) {
	    _inherits(JsonResponse, _Response);
	
	    function JsonResponse(data, success, status) {
	        _classCallCheck(this, JsonResponse);
	
	        _get(Object.getPrototypeOf(JsonResponse.prototype), 'constructor', this).call(this, data, status);
	        this._success = success;
	    }
	
	    _createClass(JsonResponse, [{
	        key: 'send',
	        value: function send() {
	            this._response.status(this.status).json(this.data);
	        }
	    }, {
	        key: 'data',
	        get: function get() {
	            return {
	                data: this._data,
	                success: this._success
	            };
	        }
	    }]);
	
	    return JsonResponse;
	})(_ResponseJs2['default']);
	
	exports['default'] = JsonResponse;
	module.exports = exports['default'];

/***/ },
/* 9 */
/***/ function(module, exports) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Response = (function () {
	    function Response(data, status) {
	        _classCallCheck(this, Response);
	
	        this._data = data;
	        this.status = status;
	    }
	
	    _createClass(Response, [{
	        key: "send",
	        value: function send() {
	            this._response.status(this.status).send(this._data);
	        }
	    }, {
	        key: "response",
	        set: function set(req) {
	            this._response = req;
	        }
	    }, {
	        key: "data",
	        get: function get() {}
	    }]);
	
	    return Response;
	})();
	
	exports["default"] = Response;
	module.exports = exports["default"];

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _mongoose = __webpack_require__(11);
	
	var _mongoose2 = _interopRequireDefault(_mongoose);
	
	var _config = __webpack_require__(12);
	
	var _config2 = _interopRequireDefault(_config);
	
	var _helperUrlHelperJs = __webpack_require__(16);
	
	var _helperUrlHelperJs2 = _interopRequireDefault(_helperUrlHelperJs);
	
	var ConnectionManager = (function () {
	    function ConnectionManager() {
	        _classCallCheck(this, ConnectionManager);
	
	        this.connections = {};
	    }
	
	    _createClass(ConnectionManager, [{
	        key: 'createConnection',
	        value: function createConnection(args) {
	            return this.get(_helperUrlHelperJs2['default'].build(args));
	        }
	    }, {
	        key: 'get',
	        value: function get(url) {
	            if (!this.connections.hasOwnProperty(url)) {
	                this.connections[url] = _mongoose2['default'].createConnection(ConnectionManager.getUrl(url));
	            }
	
	            return this.connections[url];
	        }
	    }, {
	        key: 'application',
	        get: function get() {
	            return this.get(_helperUrlHelperJs2['default'].build([_config2['default'].application.user, _config2['default'].application.project]));
	        }
	    }], [{
	        key: 'getUrl',
	        value: function getUrl(collection) {
	            var credentials = arguments.length <= 1 || arguments[1] === undefined ? false : arguments[1];
	
	            if (!credentials) {
	                return 'mongodb://localhost/' + collection;
	            } else {
	                return 'mongodb://localhost/' + collection;
	            }
	        }
	    }]);
	
	    return ConnectionManager;
	})();
	
	exports['default'] = new ConnectionManager();
	module.exports = exports['default'];

/***/ },
/* 11 */
/***/ function(module, exports) {

	module.exports = require("mongoose");

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _applicationJs = __webpack_require__(13);
	
	var _applicationJs2 = _interopRequireDefault(_applicationJs);
	
	var _applicationLocalJs = __webpack_require__(14);
	
	var _applicationLocalJs2 = _interopRequireDefault(_applicationLocalJs);
	
	var _fs = __webpack_require__(15);
	
	var _fs2 = _interopRequireDefault(_fs);
	
	function merge(o1, o2) {
	    Object.keys(o2).forEach(function (prop) {
	        var val = o2[prop];
	        if (o1.hasOwnProperty(prop)) {
	            if (typeof val == 'object') {
	                if (val && val.constructor != Array) {
	                    val = merge(o1[prop], val);
	                }
	            }
	        }
	        o1[prop] = val;
	    });
	    return o1;
	}
	exports['default'] = merge(_applicationJs2['default'], _applicationLocalJs2['default']);
	module.exports = exports['default'];

/***/ },
/* 13 */
/***/ function(module, exports) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports["default"] = {
	    application: {
	        id: "pockemon",
	        user: "pockemon",
	        project: "api",
	        version: 1
	    },
	    project: {
	        data: {
	            role: [{
	                name: "Owner",
	                alias: 'owner',
	                value: 0
	            }, {
	                name: "Admin",
	                alias: 'admin',
	                value: 1
	            }, {
	                name: "Guest",
	                alias: 'guest',
	                value: Number.MAX_VALUE
	            }]
	        },
	        models: [{
	            name: "User",
	            alias: "user",
	            structure: {
	                username: {
	                    type: 'string',
	                    "default": ''
	                },
	                gcmID: {
	                    type: 'string',
	                    "default": ''
	                },
	                oauth: {
	                    type: 'array',
	                    "default": []
	                }
	            }
	        }, {
	            name: "Role",
	            alias: "role",
	            structure: {
	                name: {
	                    type: 'string',
	                    "default": ''
	                },
	                value: {
	                    type: 'integer',
	                    "default": 10
	                }
	            }
	        }]
	    },
	    system: {
	        models: {
	            __models: {
	                name: "__Models",
	                alias: "__models",
	                type: 'system',
	                structure: {
	                    name: {
	                        type: 'string',
	                        "default": ''
	                    },
	                    alias: {
	                        type: 'string',
	                        "default": ''
	                    },
	                    type: {
	                        type: 'string',
	                        "default": ''
	                    },
	                    structure: {
	                        type: 'object',
	                        "default": {}
	                    },
	                    permissions: {
	                        type: 'object',
	                        "default": {}
	                    }
	                }
	            }
	        }
	    }
	};
	module.exports = exports["default"];

/***/ },
/* 14 */
/***/ function(module, exports) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports["default"] = {
	    application: {
	        id: "adlab_tms_api",
	        user: "adlab",
	        project: "tms",
	        port: 9999
	    }
	};
	module.exports = exports["default"];

/***/ },
/* 15 */
/***/ function(module, exports) {

	module.exports = require("fs");

/***/ },
/* 16 */
/***/ function(module, exports) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var UrlHelper = (function () {
	    function UrlHelper() {
	        _classCallCheck(this, UrlHelper);
	    }
	
	    _createClass(UrlHelper, null, [{
	        key: 'build',
	        value: function build(params) {
	            return Array.isArray(params) ? params.join('_') : 'default_';
	        }
	    }]);
	
	    return UrlHelper;
	})();
	
	exports['default'] = UrlHelper;
	module.exports = exports['default'];

/***/ },
/* 17 */
/***/ function(module, exports) {

	module.exports = require("http");

/***/ },
/* 18 */
/***/ function(module, exports) {

	module.exports = require("socket.io");

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _responseErrorResponseJs = __webpack_require__(7);
	
	var _responseErrorResponseJs2 = _interopRequireDefault(_responseErrorResponseJs);
	
	exports['default'] = function (err, req, res, next) {
	    console.log(err);
	    res.response(new _responseErrorResponseJs2['default'](err.message, {
	        stack: err.stack
	    }, false, 500));
	};
	
	module.exports = exports['default'];

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 9/7/15.
	 */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _config = __webpack_require__(12);
	
	var _config2 = _interopRequireDefault(_config);
	
	var RolesStore = (function () {
	    function RolesStore() {
	        _classCallCheck(this, RolesStore);
	
	        this.roles = _config2['default'].project.data.role;
	    }
	
	    _createClass(RolesStore, [{
	        key: 'add',
	        value: function add(role) {
	            this.roles.push(role);
	        }
	    }]);
	
	    return RolesStore;
	})();
	
	exports['default'] = new RolesStore();
	module.exports = exports['default'];

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _randomstring = __webpack_require__(22);
	
	var _randomstring2 = _interopRequireDefault(_randomstring);
	
	var _promise = __webpack_require__(23);
	
	var _promise2 = _interopRequireDefault(_promise);
	
	var _EntityManagerJs = __webpack_require__(24);
	
	var _EntityManagerJs2 = _interopRequireDefault(_EntityManagerJs);
	
	var TokenService = (function () {
	    function TokenService() {
	        _classCallCheck(this, TokenService);
	    }
	
	    _createClass(TokenService, [{
	        key: 'getToken',
	        value: function getToken(user) {
	            var _this = this;
	
	            var Token = undefined;
	            if (_EntityManagerJs2['default'].has("Token")) {
	                Token = _EntityManagerJs2['default'].get("Token");
	            } else {
	                throw new Error("Token Entity does not found");
	            }
	
	            return new _promise2['default'](function (resolve, reject) {
	                Token.findOne({ user: user._id }).exec(function (error, token) {
	                    if (!error) {
	                        if (!token) {
	                            _this.createToken(user).then(function (token) {
	                                resolve(token);
	                            }, function (error) {
	                                return reject(error);
	                            });
	                        } else {
	                            resolve(token);
	                        }
	                    } else {
	                        reject(error);
	                    }
	                });
	            });
	        }
	    }, {
	        key: 'createToken',
	        value: function createToken(user) {
	            var Token = undefined;
	            if (_EntityManagerJs2['default'].has("Token")) {
	                Token = _EntityManagerJs2['default'].get("Token");
	            } else {
	                throw new Error("Token Entity does not found");
	            }
	
	            return new _promise2['default'](function (resolve, reject) {
	                var token = new Token({
	                    token: _randomstring2['default'].generate(32),
	                    user: user
	                });
	
	                token.save(function (err, token) {
	                    if (!err) {
	                        resolve(token);
	                    } else {
	                        reject(err);
	                    }
	                });
	            });
	        }
	    }]);
	
	    return TokenService;
	})();
	
	exports['default'] = new TokenService();
	module.exports = exports['default'];

/***/ },
/* 22 */
/***/ function(module, exports) {

	module.exports = require("randomstring");

/***/ },
/* 23 */
/***/ function(module, exports) {

	module.exports = require("promise");

/***/ },
/* 24 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var EntityManager = (function () {
	    function EntityManager() {
	        _classCallCheck(this, EntityManager);
	
	        this.hash = {};
	    }
	
	    _createClass(EntityManager, [{
	        key: "add",
	        value: function add(entity) {
	            var name = arguments.length <= 1 || arguments[1] === undefined ? false : arguments[1];
	
	            this.hash[name || entity.constructor.name] = entity;
	        }
	    }, {
	        key: "has",
	        value: function has(name) {
	            return !!this.get(name);
	        }
	    }, {
	        key: "get",
	        value: function get(name) {
	            return this.hash[name];
	        }
	    }]);
	
	    return EntityManager;
	})();
	
	exports["default"] = new EntityManager();
	module.exports = exports["default"];

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _documentModelJs = __webpack_require__(26);
	
	var _documentProjectJs = __webpack_require__(28);
	
	var _express = __webpack_require__(1);
	
	var _documentProjectJs2 = _interopRequireDefault(_documentProjectJs);
	
	var _config = __webpack_require__(12);
	
	var _config2 = _interopRequireDefault(_config);
	
	var _GeneratorJs = __webpack_require__(29);
	
	var _GeneratorJs2 = _interopRequireDefault(_GeneratorJs);
	
	var _middlewareUserJs = __webpack_require__(35);
	
	var _middlewareUserJs2 = _interopRequireDefault(_middlewareUserJs);
	
	var _controllerApiCoreControllerJs = __webpack_require__(36);
	
	var _controllerApiCoreControllerJs2 = _interopRequireDefault(_controllerApiCoreControllerJs);
	
	var _controllerAuthControllerJs = __webpack_require__(44);
	
	var _controllerAuthControllerJs2 = _interopRequireDefault(_controllerAuthControllerJs);
	
	var _controllerOAuthControllerJs = __webpack_require__(45);
	
	var _controllerOAuthControllerJs2 = _interopRequireDefault(_controllerOAuthControllerJs);
	
	var _serviceConnectionManagerJs = __webpack_require__(10);
	
	var _serviceConnectionManagerJs2 = _interopRequireDefault(_serviceConnectionManagerJs);
	
	var _helperUrlHelperJs = __webpack_require__(16);
	
	var _helperUrlHelperJs2 = _interopRequireDefault(_helperUrlHelperJs);
	
	var _promise = __webpack_require__(23);
	
	var _promise2 = _interopRequireDefault(_promise);
	
	var _EntityManagerJs = __webpack_require__(24);
	
	var _EntityManagerJs2 = _interopRequireDefault(_EntityManagerJs);
	
	var _documentRoleJs = __webpack_require__(46);
	
	var _documentRoleJs2 = _interopRequireDefault(_documentRoleJs);
	
	var _storeRolesStoreJs = __webpack_require__(20);
	
	var _storeRolesStoreJs2 = _interopRequireDefault(_storeRolesStoreJs);
	
	var _documentTokenJs = __webpack_require__(47);
	
	var Bootstrap = (function () {
	    function Bootstrap() {
	        _classCallCheck(this, Bootstrap);
	    }
	
	    _createClass(Bootstrap, null, [{
	        key: 'api',
	        value: function api(env) {
	            return Bootstrap.bootstrapResources(env);
	        }
	    }, {
	        key: 'systemApi',
	        value: function systemApi(env) {
	            var connection = _serviceConnectionManagerJs2['default'].get(_helperUrlHelperJs2['default'].build([env.user, env.project]));
	            var models = _config2['default'].system.models || {};
	            var RestRouter = {};
	            Object.keys(models).forEach(function (key) {
	                var model = models[key];
	                var entity = _GeneratorJs2['default'].createSchema(connection, model);
	                if (model.type == 'system') {
	                    RestRouter = new _controllerApiCoreControllerJs2['default'](entity, model, env);
	                }
	            });
	            return RestRouter.router || (0, _express.Router)();
	        }
	    }, {
	        key: 'bootstrapResources',
	        value: function bootstrapResources(env) {
	            var connection = _serviceConnectionManagerJs2['default'].get(_helperUrlHelperJs2['default'].build([env.user, env.project]));
	            var Model = (0, _documentModelJs.make)(connection);
	            var TokenModel = (0, _documentTokenJs.make)(connection);
	            _EntityManagerJs2['default'].add(TokenModel, "Token");
	            _EntityManagerJs2['default'].add(Model, "Model");
	
	            var router = (0, _express.Router)();
	            return _promise2['default'].all([new _promise2['default'](function (resolve, reject) {
	
	                router.use('*', (0, _middlewareUserJs2['default'])(TokenModel));
	
	                Model.find({}).exec(function (err, models) {
	                    if (err) {
	                        reject(err);
	                    } else {
	                        models.forEach(function (model) {
	                            var entity = _GeneratorJs2['default'].createSchema(connection, model);
	                            switch (model.type) {
	                                case _documentModelJs.TYPES.USER_ENTITY:
	                                    var Auth = new _controllerAuthControllerJs2['default'](entity, model, env);
	                                    var OAuth = new _controllerOAuthControllerJs2['default'](entity, model, env);
	
	                                    router.use(Auth.router);
	                                    router.use(OAuth.router);
	                                    break;
	                                default:
	                                    var RestRouter = new _controllerApiCoreControllerJs2['default'](entity, model, env);
	                                    router.use(RestRouter.router);
	                            }
	                        });
	                    }
	                    resolve(router);
	                });
	            }), new _promise2['default'](function (resolve, reject) {
	                _documentRoleJs2['default'].find({}).exec(function (err, models) {
	                    models.forEach(function (model) {
	                        return _storeRolesStoreJs2['default'].add(model);
	                    });
	                    resolve(true);
	                });
	            })]).then(function (results) {
	                return results[0];
	            });
	        }
	    }, {
	        key: 'init',
	        value: function init(env) {
	            return Bootstrap.api(env);
	        }
	    }]);
	
	    return Bootstrap;
	})();
	
	exports['default'] = Bootstrap;
	module.exports = exports['default'];

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	exports.make = make;
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _mongoose = __webpack_require__(11);
	
	var _mongoose2 = _interopRequireDefault(_mongoose);
	
	var _mongooseTimestamp = __webpack_require__(27);
	
	var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);
	
	var _config = __webpack_require__(12);
	
	var _config2 = _interopRequireDefault(_config);
	
	var _serviceConnectionManagerJs = __webpack_require__(10);
	
	var _serviceConnectionManagerJs2 = _interopRequireDefault(_serviceConnectionManagerJs);
	
	var Schema = _mongoose2['default'].Schema,
	    ObjectId = Schema.ObjectId;
	
	var Model = new Schema({
	    name: String,
	    alias: String,
	    permissions: {},
	    structure: {},
	    meta: {},
	    type: String
	}, { collection: "__models" });
	
	Model.plugin(_mongooseTimestamp2['default']);
	
	exports['default'] = _serviceConnectionManagerJs2['default'].application.model('Model', Model);
	var TYPES = {
	    USER_ENTITY: "USER_ENTITY",
	    API_ENTITY: "API_ENTITY"
	};
	exports.TYPES = TYPES;
	
	function make(connection) {
	    return connection.model('Model', Model);
	}

/***/ },
/* 27 */
/***/ function(module, exports) {

	module.exports = require("mongoose-timestamp");

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	exports.make = make;
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _mongoose = __webpack_require__(11);
	
	var _mongoose2 = _interopRequireDefault(_mongoose);
	
	var _mongooseTimestamp = __webpack_require__(27);
	
	var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);
	
	var _config = __webpack_require__(12);
	
	var _config2 = _interopRequireDefault(_config);
	
	var _serviceConnectionManagerJs = __webpack_require__(10);
	
	var _serviceConnectionManagerJs2 = _interopRequireDefault(_serviceConnectionManagerJs);
	
	var Schema = _mongoose2['default'].Schema,
	    ObjectId = Schema.ObjectId;
	
	var Project = new Schema({
	    link: String,
	    authentication: {
	        model: { type: ObjectId, ref: 'Model' },
	        login_route: String,
	        credentials: {
	            identity: String,
	            credential: String
	        }
	    }
	}, { collection: "__project" });
	
	Project.plugin(_mongooseTimestamp2['default']);
	
	exports['default'] = _serviceConnectionManagerJs2['default'].application.model('Project', Project);
	
	function make(connection) {
	    return connection.model('Model', Project);
	}

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _TypeCheckerJs = __webpack_require__(30);
	
	var _TypeCheckerJs2 = _interopRequireDefault(_TypeCheckerJs);
	
	var _documentModelJs = __webpack_require__(26);
	
	var _passport = __webpack_require__(4);
	
	var _passport2 = _interopRequireDefault(_passport);
	
	var _mongoose = __webpack_require__(11);
	
	var _mongoose2 = _interopRequireDefault(_mongoose);
	
	var _helperAbstractEntityJs = __webpack_require__(31);
	
	var _helperAbstractEntityJs2 = _interopRequireDefault(_helperAbstractEntityJs);
	
	var _EntityManagerJs = __webpack_require__(24);
	
	var _EntityManagerJs2 = _interopRequireDefault(_EntityManagerJs);
	
	var _documentUserJs = __webpack_require__(32);
	
	var _passportLocal = __webpack_require__(34);
	
	var _passportLocal2 = _interopRequireDefault(_passportLocal);
	
	var Schema = _mongoose2['default'].Schema,
	    ObjectId = Schema.ObjectId;
	
	var Generator = (function () {
	    function Generator() {
	        _classCallCheck(this, Generator);
	    }
	
	    _createClass(Generator, null, [{
	        key: 'createSchema',
	        value: function createSchema(connection, model) {
	            var name = model.name;
	            var structure = model.structure;
	            var alias = model.alias;
	
	            var schema = {};
	            if (structure) {
	
	                Object.keys(structure).forEach(function (property) {
	                    var description = structure[property];
	                    schema[property] = _TypeCheckerJs2['default'].convert(description.type, description);
	                });
	                var MongooseModel = {};
	                switch (model.type) {
	                    case _documentModelJs.TYPES.USER_ENTITY:
	                        {
	                            _documentUserJs.UserSchema.plugin((0, _documentUserJs.extend)(schema));
	                            MongooseModel = connection.model(alias, _documentUserJs.UserSchema);
	                            break;
	                        }
	                    default:
	                        {
	                            var Model = new Schema(schema);
	                            Model.plugin(_helperAbstractEntityJs2['default']);
	                            MongooseModel = connection.model(alias, Model);
	                            break;
	                        }
	                }
	                _EntityManagerJs2['default'].add(MongooseModel, model.name);
	                return MongooseModel;
	            } else {
	                throw "Schema not found!";
	            }
	        }
	    }]);
	
	    return Generator;
	})();
	
	exports['default'] = Generator;
	module.exports = exports['default'];

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _mongoose = __webpack_require__(11);
	
	var _mongoose2 = _interopRequireDefault(_mongoose);
	
	var Schema = _mongoose2['default'].Schema,
	    ObjectId = Schema.ObjectId;
	
	var TypeChecker = (function () {
	    function TypeChecker() {
	        _classCallCheck(this, TypeChecker);
	    }
	
	    _createClass(TypeChecker, null, [{
	        key: 'convert',
	        value: function convert(type, meta) {
	            switch (type) {
	                case TypeChecker.TYPES.STRING:
	                    return { type: String, 'default': meta['default'] };
	                case TypeChecker.TYPES.NUMBER:
	                    return { type: Number, 'default': meta['default'] };
	                case TypeChecker.TYPES.DATE:
	                    return { type: Date, 'default': meta['default'] };
	                case TypeChecker.TYPES.ARRAY:
	                    return { type: Array, 'default': meta['default'] };
	                case TypeChecker.TYPES.OBJECT:
	                    return typeof meta['default'] == 'object' ? { type: Object, 'default': meta['default'] } : {};
	                case TypeChecker.TYPES.REF:
	                    return { type: ObjectId, ref: meta.ref };
	                case TypeChecker.TYPES.LIST:
	                    return [{ type: ObjectId, ref: meta.ref }];
	                case TypeChecker.TYPES.PREBIND:
	                    return { entity: { type: ObjectId, ref: meta.ref }, metadata: {} };
	                case TypeChecker.TYPES.PREBIND_LIST:
	                    return [{ entity: { type: ObjectId, ref: meta.ref }, metadata: {} }];
	            }
	        }
	    }]);
	
	    return TypeChecker;
	})();
	
	TypeChecker.TYPES = {
	    STRING: 'string',
	    MIDDLE: 'middle',
	    PREBIND: 'prebind',
	    PREBIND_LIST: 'prebind_list',
	    NUMBER: 'number',
	    OBJECT: 'object',
	    ARRAY: 'array',
	    DATE: 'date',
	    REF: 'ref',
	    LIST: 'list'
	};
	
	exports['default'] = TypeChecker;
	module.exports = exports['default'];

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 9/9/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _mongoose = __webpack_require__(11);
	
	var _mongoose2 = _interopRequireDefault(_mongoose);
	
	var _mongooseTimestamp = __webpack_require__(27);
	
	var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);
	
	var Schema = _mongoose2['default'].Schema,
	    ObjectId = Schema.ObjectId;
	
	exports['default'] = function (schema, opt) {
	    schema.plugin(_mongooseTimestamp2['default']);
	    schema.add({
	        creator: { ref: "User", type: ObjectId },
	        __status: { type: Boolean, 'default': true }
	    });
	};
	
	module.exports = exports['default'];

/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	exports.extend = extend;
	exports.make = make;
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var _mongoose = __webpack_require__(11);
	
	var _mongoose2 = _interopRequireDefault(_mongoose);
	
	var _mongooseTimestamp = __webpack_require__(27);
	
	var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);
	
	var _passportLocalMongoose = __webpack_require__(33);
	
	var _passportLocalMongoose2 = _interopRequireDefault(_passportLocalMongoose);
	
	var _config = __webpack_require__(12);
	
	var _config2 = _interopRequireDefault(_config);
	
	var _serviceConnectionManagerJs = __webpack_require__(10);
	
	var _serviceConnectionManagerJs2 = _interopRequireDefault(_serviceConnectionManagerJs);
	
	var Schema = _mongoose2['default'].Schema,
	    ObjectId = Schema.ObjectId;
	
	var User = new Schema({
	    gcmID: { type: String },
	    role: { type: String },
	    oauth: [{
	        provider: { type: String },
	        key: { type: String }
	    }]
	}, { collection: "users" });
	
	User.plugin(_mongooseTimestamp2['default']);
	User.plugin(_passportLocalMongoose2['default'], {
	    usernameUnique: true
	});
	
	User.methods.toJSON = function () {
	    var obj = this.toObject();
	    delete obj.salt;
	    delete obj.hash;
	    delete obj.password;
	    return obj;
	};
	
	exports['default'] = _serviceConnectionManagerJs2['default'].application.model('User', User);
	
	function extend(schema) {
	    return function (sch, options) {
	        Object.keys(schema).forEach(function (p) {
	            sch.add(_defineProperty({}, p, schema[p]));
	        });
	    };
	}
	
	var UserSchema = User;
	exports.UserSchema = UserSchema;
	
	function make(connection) {
	    return connection.model('User', User);
	}

/***/ },
/* 33 */
/***/ function(module, exports) {

	module.exports = require("passport-local-mongoose");

/***/ },
/* 34 */
/***/ function(module, exports) {

	module.exports = require("passport-local");

/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _responseErrorResponseJs = __webpack_require__(7);
	
	var _responseErrorResponseJs2 = _interopRequireDefault(_responseErrorResponseJs);
	
	exports['default'] = function (Token) {
	    return function (req, res, next) {
	        var token = req.headers.token;
	        var app = req.headers.app;
	        if (!token) {
	            req.token = false;
	            req.userRole = 'guest';
	            req.user = {
	                name: "Guest",
	                type: 'guest',
	                role: 'guest'
	            };
	            next();
	        } else {
	            Token.findOne({ token: token }).populate('user').exec(function (error, token) {
	                if (token && token != null) {
	                    req.token = token;
	                    req.user = token.user;
	                    req.userRole = req.user.role;
	                    next();
	                } else {
	                    res.status(403).json({ success: false, message: 'Token for is not found' });
	                }
	            });
	        }
	    };
	};
	
	module.exports = exports['default'];

/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/12/15.
	 */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	var _get = function get(_x2, _x3, _x4) { var _again = true; _function: while (_again) { var object = _x2, property = _x3, receiver = _x4; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x2 = parent; _x3 = property; _x4 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var _express = __webpack_require__(1);
	
	var _storeRolesStoreJs = __webpack_require__(20);
	
	var _storeRolesStoreJs2 = _interopRequireDefault(_storeRolesStoreJs);
	
	var _middlewareActionJs = __webpack_require__(37);
	
	var _middlewareActionJs2 = _interopRequireDefault(_middlewareActionJs);
	
	var _helperModelActionHelperJs = __webpack_require__(38);
	
	var _helperModelActionHelperJs2 = _interopRequireDefault(_helperModelActionHelperJs);
	
	var _RestControllerJs = __webpack_require__(39);
	
	var _RestControllerJs2 = _interopRequireDefault(_RestControllerJs);
	
	var ApiCoreController = (function (_RestController) {
	    _inherits(ApiCoreController, _RestController);
	
	    function ApiCoreController(entity, model) {
	        var metadata = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];
	
	        _classCallCheck(this, ApiCoreController);
	
	        _get(Object.getPrototypeOf(ApiCoreController.prototype), 'constructor', this).call(this, entity, model.prebindings, model);
	
	        this.router = (0, _express.Router)();
	        this.model = model;
	        this.metadata = metadata;
	
	        this.url = '/' + metadata.user + '/' + metadata.project + '/api/:version/' + model.alias + '/:id?';
	        this.router.use(this.url, _middlewareActionJs2['default']);
	        this.router.use(this.url, this.check.bind(this));
	
	        this.router.get(this.url, this.get.bind(this));
	        this.router.post(this.url, this.post.bind(this));
	        this.router.put(this.url, this.post.bind(this));
	        this.router.patch(this.url, this._patch.bind(this));
	        this.router['delete'](this.url, this._delete.bind(this));
	    }
	
	    _createClass(ApiCoreController, [{
	        key: 'check',
	        value: function check(req, res, next) {
	            req.__version = req.params.version;
	            var permissions = this.model.permissions || {};
	            var actionPermissions = permissions[req.modelAction] || {};
	            var userPermissions = actionPermissions[req.userRole] || false;
	            if (Array.isArray(userPermissions)) {
	                (function () {
	                    var orQuery = [];
	                    userPermissions.forEach(function (field) {
	                        orQuery.push(_defineProperty({}, field, req.user._id));
	                        orQuery.push(_defineProperty({}, field, {
	                            $in: [req.user._id]
	                        }));
	                    });
	                    req.queryCliteria = orQuery;
	                })();
	            }
	
	            req.actionPermissions = {
	                all: permissions,
	                action: actionPermissions,
	                user: userPermissions
	            };
	
	            if (userPermissions || userPermissions == '*') {
	                next();
	            } else {
	                res.status(403).json({ success: false, permissions: req.actionPermissions, message: "Permissions denied." });
	            }
	        }
	    }, {
	        key: 'get',
	        value: function get(req, res, next) {
	            var id = req.params.id;
	
	            if (id) {
	                this.single(req.params.id, req, res, next);
	            } else {
	                this.list(req, res, next);
	            }
	        }
	    }, {
	        key: 'post',
	        value: function post(req, res, next) {
	            var data = req.body,
	                id = req.params.id;
	            if (!id) {
	                this.create(data, req, res, next);
	            } else {
	                this.update(id, data, req, res, next);
	            }
	        }
	    }, {
	        key: '_patch',
	        value: function _patch(req, res, next) {
	            var data = req.body,
	                id = req.params.id;
	            if (id) {
	                this.patch(id, data, req, res, next);
	            } else {
	                res.json({ success: false });
	            }
	        }
	    }, {
	        key: '_delete',
	        value: function _delete(req, res, next) {
	            this['delete'](req.params.id, req, res, next);
	        }
	    }]);
	
	    return ApiCoreController;
	})(_RestControllerJs2['default']);
	
	exports['default'] = ApiCoreController;
	module.exports = exports['default'];

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 9/9/15.
	 */
	
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _helperModelActionHelperJs = __webpack_require__(38);
	
	var _helperModelActionHelperJs2 = _interopRequireDefault(_helperModelActionHelperJs);
	
	exports['default'] = function (req, res, next) {
	    req.__version = req.params.version;
	    var id = req.params.id;
	    switch (req.method.toLocaleLowerCase()) {
	        case 'get':
	            {
	                req.modelAction = id ? _helperModelActionHelperJs2['default'].READ_SINGLE : _helperModelActionHelperJs2['default'].READ_LIST;
	                break;
	            }
	        case 'post':
	            {
	                req.modelAction = id ? _helperModelActionHelperJs2['default'].UPDATE : _helperModelActionHelperJs2['default'].CREATE;
	                break;
	            }
	        case 'patch':
	            {
	                req.modelAction = id ? _helperModelActionHelperJs2['default'].UPDATE : null;
	                break;
	            }
	        case 'delete':
	            {
	                req.modelAction = _helperModelActionHelperJs2['default'].DELETE;
	                break;
	            }
	    }
	
	    next();
	};
	
	module.exports = exports['default'];

/***/ },
/* 38 */
/***/ function(module, exports) {

	/**
	 * Created by igor.olshevsky on 9/6/15.
	 */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	exports['default'] = {
	    CREATE: 'create',
	
	    READ: 'read',
	    READ_SINGLE: 'read_single',
	    READ_LIST: 'read_list',
	
	    UPDATE: 'update',
	    DELETE: 'delete'
	};
	module.exports = exports['default'];

/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/12/15.
	 */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	var _get = function get(_x2, _x3, _x4) { var _again = true; _function: while (_again) { var object = _x2, property = _x3, receiver = _x4; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x2 = parent; _x3 = property; _x4 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var _ControllerJs = __webpack_require__(40);
	
	var _ControllerJs2 = _interopRequireDefault(_ControllerJs);
	
	var _validatorJs = __webpack_require__(41);
	
	var _helperValidationFactoryJs = __webpack_require__(42);
	
	var _helperValidationFactoryJs2 = _interopRequireDefault(_helperValidationFactoryJs);
	
	var RestController = (function (_Controller) {
	    _inherits(RestController, _Controller);
	
	    function RestController(entity, prebinding) {
	        var schema = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];
	
	        _classCallCheck(this, RestController);
	
	        _get(Object.getPrototypeOf(RestController.prototype), 'constructor', this).call(this);
	
	        this.entity = entity;
	        this.schema = schema;
	        this.validator = _helperValidationFactoryJs2['default'].getValidation(schema.structure);
	        this.prebinding = prebinding || '';
	    }
	
	    _createClass(RestController, [{
	        key: 'single',
	        value: function single(id, req, res, next) {
	            var query = this.entity.findOne({ _id: id });
	            if (req.queryCliteria) {
	                query.or(req.queryCliteria);
	            }
	
	            query.populate(this.prebinding).exec(function (err, model) {
	                if (err) {
	                    res.json(err);
	                } else {
	                    if (model && model != null) {
	                        res.json({ model: model });
	                    } else {
	                        res.status(404).json({ success: false, message: "Not Found" });
	                    }
	                }
	            });
	        }
	    }, {
	        key: 'list',
	        value: function list(req, res, next) {
	            var query = this.entity.find({});
	
	            if (req.queryCliteria) {
	                query.or(req.queryCliteria);
	            }
	
	            query.populate(this.prebinding).exec(function (err, model) {
	                if (err) {
	                    res.json(err);
	                } else {
	                    if (model && model != null) {
	                        res.json(model);
	                    } else {
	                        res.status(404).json({ success: false, message: "Not Found" });
	                    }
	                }
	            });
	        }
	    }, {
	        key: 'create',
	        value: function create(data, req, res, next) {
	            var _this = this;
	
	            var validated = _helperValidationFactoryJs2['default'].validate(data, this.validator);
	            if (validated.valid) {
	                (function () {
	                    if (req.token) {
	                        data.creator = req.user._id;
	                    }
	                    var object = new _this.entity(data);
	                    object.save(function (err) {
	                        if (err) {
	                            res.json(err);
	                        } else {
	                            res.json(object);
	                        }
	                    });
	                })();
	            } else {
	                res.json({ success: false, error: validated });
	            }
	        }
	    }, {
	        key: 'update',
	        value: function update(id, data, req, res, next) {
	            var _this2 = this;
	
	            var filter = _helperValidationFactoryJs2['default'].validate(data, this.validator);
	            if (filter.valid) {
	                var query = this.entity.findOne({ _id: id });
	                if (req.queryCliteria) {
	                    query.or(req.queryCliteria);
	                }
	                query.populate(this.prebinding).exec(function (err, model) {
	                    if (err) {
	                        res.json(err);
	                    } else {
	                        if (model && model != null) {
	                            Object.keys(filter.map).forEach(function (key) {
	                                model[key] = data[key];
	                            });
	                            model._id = model._id.toString();
	                            _this2.entity.update(model, function (err) {
	                                if (err) {
	                                    res.json(err);
	                                } else {
	                                    res.json(model);
	                                }
	                            });
	                        } else {
	                            res.status(404).json({ success: false, message: "Not Found" });
	                        }
	                    }
	                });
	            } else {
	                res.json({ success: false, filter: filter });
	            }
	        }
	    }, {
	        key: 'patch',
	        value: function patch(id, data, req, res, next) {
	            var _this3 = this;
	
	            var filter = _helperValidationFactoryJs2['default'].setValidationGroup(data, Object.keys(data), this.validator);
	
	            if (filter.valid) {
	                var query = this.entity.findOne({ _id: id });
	                if (req.queryCliteria) {
	                    query.or(req.queryCliteria);
	                }
	                query.populate(this.prebinding).exec(function (err, model) {
	                    if (err) {
	                        res.json(err);
	                    } else {
	                        if (model && model != null) {
	                            Object.keys(filter.map).forEach(function (key) {
	                                model[key] = req.body[key];
	                            });
	                            model._id = model._id.toString();
	                            _this3.entity.update(model, function (err) {
	                                if (err) {
	                                    res.json(err);
	                                } else {
	                                    res.json(model);
	                                }
	                            });
	                        } else {
	                            res.status(404).json({ success: false, message: "Not Found" });
	                        }
	                    }
	                });
	            } else {
	                res.json({ success: false, error: filter });
	            }
	        }
	    }, {
	        key: 'delete',
	        value: function _delete(id, req, res, next) {
	            var query = this.entity.findOne(id);
	            if (req.queryCliteria) {
	                query.or(req.queryCliteria);
	            }
	            query.remove().exec(function (err) {
	                if (err) {
	                    res.json({ success: false });
	                } else {
	                    res.json({ _id: id, success: true });
	                }
	            });
	        }
	    }]);
	
	    return RestController;
	})(_ControllerJs2['default']);
	
	exports['default'] = RestController;
	module.exports = exports['default'];

/***/ },
/* 40 */
/***/ function(module, exports) {

	/**
	 * Created by igor.olshevsky on 8/12/15.
	 */
	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Controller = function Controller() {
	  _classCallCheck(this, Controller);
	};
	
	exports["default"] = Controller;
	module.exports = exports["default"];

/***/ },
/* 41 */
/***/ function(module, exports) {

	module.exports = require("validator.js");

/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/28/15.
	 */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _validator = __webpack_require__(43);
	
	var _validator2 = _interopRequireDefault(_validator);
	
	var ValidationFactory = (function () {
	    function ValidationFactory() {
	        _classCallCheck(this, ValidationFactory);
	    }
	
	    _createClass(ValidationFactory, null, [{
	        key: 'getValidation',
	        value: function getValidation(structure) {
	            var validatorsHash = {};
	            if (structure) {
	                Object.keys(structure).map(function (property) {
	                    var propertyValidators = [];
	                    var description = structure[property];
	                    if (description.hasOwnProperty('validators') && Array.isArray(description.validators)) {
	                        propertyValidators = description.validators.map(function (validator) {
	                            return {
	                                type: validator.type,
	                                validate: ValidationFactory.convertValidator(validator)
	                            };
	                        });
	                    }
	                    validatorsHash[property] = propertyValidators;
	                });
	            }
	
	            return validatorsHash;
	        }
	    }, {
	        key: 'validate',
	        value: function validate(data, validator) {
	            var validationResult = {},
	                kv = {},
	                resultsArray = [];
	
	            Object.keys(validator).map(function (key) {
	                var propertyValid = true;
	                var propertyValidationResult = {};
	                validator[key].map(function (v) {
	                    propertyValid = v.validate(data[key]);
	                    propertyValidationResult[v.type] = propertyValid;
	                });
	                kv[key] = propertyValid;
	                resultsArray.push(propertyValid);
	                validationResult[key] = propertyValidationResult;
	            });
	
	            return {
	                valid: !resultsArray.filter(function (r) {
	                    return !r;
	                }).length,
	                map: kv,
	                properties: validationResult
	            };
	        }
	    }, {
	        key: 'carry',
	        value: function carry(validator) {
	            for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
	                args[_key - 1] = arguments[_key];
	            }
	
	            return function (value) {
	                return validator(value, args);
	            };
	        }
	    }, {
	        key: 'setValidationGroup',
	        value: function setValidationGroup(data, group, validator) {
	            Object.keys(validator).forEach(function (field) {
	                if (group.indexOf(field) == -1) {
	                    delete validator[field];
	                };
	            });
	            return ValidationFactory.validate(data, validator);
	        }
	    }, {
	        key: 'convertValidator',
	        value: function convertValidator(desc) {
	            var type = desc.type;
	            var metadata = desc.metadata;
	
	            metadata = metadata || {};
	            switch (type) {
	                case ValidationFactory.TYPES.CONTAINS:
	                    {
	                        return ValidationFactory.carry(_validator2['default'].contains, metadata.seed);
	                    }
	                case ValidationFactory.TYPES.AFTER:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.ALPHA:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.ALPHANUMERIC:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.ASCII:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.BASE64:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.BEFORE:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.BOOLEAN:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.BYTE_LENGTH:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.CREDIT_CARD:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.CURRENCY:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.DATE:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.DECIMAL:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.DIVISIBLE_BY:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.EMAIL:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.FQDN:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.FLOAT:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.FULLWIDTH:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.HALFWIDTH:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.HEX_COLOR:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.HEXADECIMAL:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.IP:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.ISBN:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.ISO8601:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.IN:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.JSON:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.LENGTH:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.LOWERCASE:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.MONGO_ID:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.MULTIBYTE:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.NULL:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.NUMERIC:
	                    {
	                        return ValidationFactory.carry(_validator2['default'].isNumeric);
	                    }
	                case ValidationFactory.TYPES.URL:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.UUID:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.UPPERCASE:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.VARIABLE_WIDTH:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.MATCHES:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.INT:
	                    {
	                        return;
	                    }
	                case ValidationFactory.TYPES.EQUALS:
	                    {
	                        return;
	                    }
	                default:
	                    return;
	            }
	        }
	    }]);
	
	    return ValidationFactory;
	})();
	
	ValidationFactory.TYPES = {
	    CONTAINS: 'contains',
	    AFTER: 'after',
	    ALPHA: 'alpha',
	    ALPHANUMERIC: 'alphanumeric',
	    ASCII: 'ascii',
	    BASE64: 'base64',
	    BEFORE: 'before',
	    BOOLEAN: 'boolean',
	    BYTE_LENGTH: 'byteLength',
	    CREDIT_CARD: 'creditCard',
	    CURRENCY: 'currency',
	    DATE: 'date',
	    DECIMAL: 'decimal',
	    DIVISIBLE_BY: 'divisibleBy',
	    EMAIL: 'email',
	    FQDN: 'qbdn',
	    FLOAT: 'float',
	    FULLWIDTH: 'fullwidth',
	    HALFWIDTH: 'halfwidth',
	    HEX_COLOR: 'hexColor',
	    HEXADECIMAL: 'hexadecimal',
	    IP: 'ip',
	    ISBN: 'isbn',
	    ISO8601: 'iso8601',
	    IN: 'in',
	    JSON: 'json',
	    LENGTH: 'length',
	    LOWERCASE: 'lowercase',
	    MONGO_ID: 'mongoId',
	    MULTIBYTE: 'mongoId',
	    NULL: 'null',
	    NUMERIC: 'numeric',
	    URL: 'url',
	    UUID: 'uuid',
	    UPPERCASE: 'uppercase',
	    VARIABLE_WIDTH: 'variableWidth',
	    MATCHES: 'matches',
	    INT: 'int',
	    EQUALS: 'equals'
	};
	
	exports['default'] = ValidationFactory;
	module.exports = exports['default'];

/***/ },
/* 43 */
/***/ function(module, exports) {

	module.exports = require("validator");

/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var _express = __webpack_require__(1);
	
	var _passport = __webpack_require__(4);
	
	var _passport2 = _interopRequireDefault(_passport);
	
	var _passportLocal = __webpack_require__(34);
	
	var _passportLocal2 = _interopRequireDefault(_passportLocal);
	
	var _documentUserJs = __webpack_require__(32);
	
	var _documentUserJs2 = _interopRequireDefault(_documentUserJs);
	
	var _RestControllerJs = __webpack_require__(39);
	
	var _RestControllerJs2 = _interopRequireDefault(_RestControllerJs);
	
	var _serviceTokenServiceJs = __webpack_require__(21);
	
	var _serviceTokenServiceJs2 = _interopRequireDefault(_serviceTokenServiceJs);
	
	var AuthController = (function (_RestController) {
	    _inherits(AuthController, _RestController);
	
	    function AuthController(entity, model, metadata) {
	        _classCallCheck(this, AuthController);
	
	        _passport2['default'].use(new _passportLocal2['default'](entity.authenticate()));
	        _passport2['default'].serializeUser(entity.serializeUser());
	        _passport2['default'].deserializeUser(entity.deserializeUser());
	
	        _get(Object.getPrototypeOf(AuthController.prototype), 'constructor', this).call(this, entity, model.prebindings);
	
	        this.entity = entity;
	        this.model = model;
	
	        this.metadata = metadata;
	        this.urls = {
	
	            auth: '/' + metadata.user + '/' + metadata.project + '/auth/api/:version/' + model.alias,
	            rest: '/' + metadata.user + '/' + metadata.project + '/api/:version/' + model.alias + '/:id?'
	        };
	
	        this.router = (0, _express.Router)();
	        //this.router.use(this.urls.auth, this.check.bind(this));
	        this.router.use(this.urls.rest, this.check.bind(this));
	
	        this.router.get(this.urls.auth, this.user.bind(this));
	        this.router.post(this.urls.auth, this.authenticate.bind(this));
	
	        this.router.get(this.urls.rest, this.get.bind(this));
	        this.router.post(this.urls.rest, this.post.bind(this));
	        this.router.put(this.urls.rest, this.post.bind(this));
	        this.router.patch(this.urls.rest, this._patch.bind(this));
	        this.router['delete'](this.urls.rest, this._delete.bind(this));
	    }
	
	    _createClass(AuthController, [{
	        key: 'check',
	        value: function check(req, res, next) {
	            req.__version = req.params.version;
	            var permissions = this.model.permissions || {};
	            var actionPermissions = permissions[req.modelAction] || {};
	            var userPermissions = actionPermissions[req.userRole] || false;
	
	            if (Array.isArray(userPermissions)) {
	                (function () {
	                    var orQuery = [];
	                    userPermissions.forEach(function (field) {
	                        orQuery.push(_defineProperty({}, field, req.user._id));
	                    });
	                    req.queryCliteria = orQuery;
	                })();
	            }
	
	            req.actionPermissions = {
	                all: permissions,
	                action: actionPermissions,
	                user: userPermissions
	            };
	
	            if (userPermissions || userPermissions == '*') {
	                next();
	            } else {
	                res.status(403).json({ success: false, message: "Permissions denied." });
	            }
	        }
	    }, {
	        key: 'get',
	        value: function get(req, res, next) {
	            var id = req.params.id;
	            if (id) {
	                if (id == 'list') {
	                    this.list(req.params.id, req, res, next);
	                } else {
	                    this.single(req.params.id, req, res, next);
	                }
	            } else {
	                res.json(req.user);
	            }
	        }
	    }, {
	        key: 'post',
	        value: function post(req, res, next) {
	            var data = req.body,
	                id = req.params.id;
	            if (!id) {
	                this.create(data, req, res, next);
	            } else {
	                this.update(id, data, req, res, next);
	            }
	        }
	    }, {
	        key: '_patch',
	        value: function _patch(req, res, next) {
	            var data = req.body,
	                id = req.params.id;
	            if (id) {
	                this.patch(id, data, req, res, next);
	            } else {
	                res.json({ success: false });
	            }
	        }
	    }, {
	        key: '_delete',
	        value: function _delete(req, res, next) {
	            this['delete'](req.params.id, req, res, next);
	        }
	    }, {
	        key: 'authenticate',
	        value: function authenticate(req, res, next) {
	            _passport2['default'].authenticate('local', { session: false }, function (err, user, info) {
	                if (user) {
	                    _serviceTokenServiceJs2['default'].getToken(user).then(function (token) {
	                        res.json({ user: user, token: token });
	                    });
	                } else {
	                    res.json({ err: err, info: info, user: user });
	                }
	            })(req, res, next);
	        }
	    }, {
	        key: 'user',
	        value: function user(req, res, next) {
	            res.json(req.user);
	        }
	    }]);
	
	    return AuthController;
	})(_RestControllerJs2['default']);
	
	exports['default'] = AuthController;
	module.exports = exports['default'];

/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by igor.olshevsky on 8/13/15.
	 */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var _express = __webpack_require__(1);
	
	var _documentUserJs = __webpack_require__(32);
	
	var _documentUserJs2 = _interopRequireDefault(_documentUserJs);
	
	var _RestControllerJs = __webpack_require__(39);
	
	var _RestControllerJs2 = _interopRequireDefault(_RestControllerJs);
	
	var _passport = __webpack_require__(4);
	
	var _passport2 = _interopRequireDefault(_passport);
	
	var _passportLocal = __webpack_require__(34);
	
	var _passportLocal2 = _interopRequireDefault(_passportLocal);
	
	var AuthController = (function (_RestController) {
	    _inherits(AuthController, _RestController);
	
	    function AuthController(entity, model, metadata) {
	        _classCallCheck(this, AuthController);
	
	        _get(Object.getPrototypeOf(AuthController.prototype), 'constructor', this).call(this, entity, '');
	        this.metadata = metadata;
	        this.urls = {
	            oauth: '/' + metadata.user + '/' + metadata.project + '/oauth/:provider',
	            callback: '/' + metadata.user + '/' + metadata.project + '/oauth/:provider/callback'
	        };
	        this.router = (0, _express.Router)();
	        this.router.use(this.urls.oauth, this.check.bind(this));
	        this.router.get(this.urls.oauth, this.oauth.bind(this));
	
	        this.router.get(this.urls.callback, this.callback.bind(this));
	    }
	
	    _createClass(AuthController, [{
	        key: 'check',
	        value: function check(req, res, next) {
	            req.__version = req.params.version;
	            next();
	        }
	    }, {
	        key: 'oauth',
	        value: function oauth(req, res, next) {
	            _passport2['default'].authenticate(req.params.provider)(req, res, next);
	        }
	    }, {
	        key: 'callback',
	        value: function callback(req, res, next) {
	            _passport2['default'].authenticate(req.params.provider, {
	                session: false,
	                failureRedirect: '/auth'
	            }, function (err, user, info) {
	                if (user) {
	                    res.json({ user: user, info: info });
	                } else {
	                    res.json({ err: err, info: info, user: user });
	                }
	            })(req, res, next);
	        }
	    }]);
	
	    return AuthController;
	})(_RestControllerJs2['default']);
	
	exports['default'] = AuthController;
	module.exports = exports['default'];

/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	exports.make = make;
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _mongoose = __webpack_require__(11);
	
	var _mongoose2 = _interopRequireDefault(_mongoose);
	
	var _mongooseTimestamp = __webpack_require__(27);
	
	var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);
	
	var _config = __webpack_require__(12);
	
	var _config2 = _interopRequireDefault(_config);
	
	var _serviceConnectionManagerJs = __webpack_require__(10);
	
	var _serviceConnectionManagerJs2 = _interopRequireDefault(_serviceConnectionManagerJs);
	
	var Schema = _mongoose2['default'].Schema,
	    ObjectId = Schema.ObjectId;
	
	var Role = new Schema({
	    name: String,
	    alias: String,
	    value: Number
	});
	
	Role.plugin(_mongooseTimestamp2['default']);
	
	exports['default'] = _serviceConnectionManagerJs2['default'].application.model('Role', Role);
	
	function make(connection) {
	    return connection.model('Role', Role);
	}

/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	exports.make = make;
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _mongoose = __webpack_require__(11);
	
	var _mongoose2 = _interopRequireDefault(_mongoose);
	
	var _mongooseTimestamp = __webpack_require__(27);
	
	var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);
	
	var _config = __webpack_require__(12);
	
	var _config2 = _interopRequireDefault(_config);
	
	var _serviceConnectionManagerJs = __webpack_require__(10);
	
	var _serviceConnectionManagerJs2 = _interopRequireDefault(_serviceConnectionManagerJs);
	
	var Schema = _mongoose2['default'].Schema,
	    ObjectId = Schema.ObjectId;
	
	var Token = new Schema({
	    token: String,
	    metadata: {},
	    user: { type: ObjectId, ref: 'User' },
	    expires: Date
	});
	
	Token.plugin(_mongooseTimestamp2['default']);
	
	exports['default'] = _serviceConnectionManagerJs2['default'].application.model('Token', Token);
	
	function make(connection) {
	    return connection.model('Token', Token);
	}

/***/ },
/* 48 */
/***/ function(module, exports) {

	module.exports = require("express-session");

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	(function () {
	
	  'use strict';
	
	  var vary = __webpack_require__(50);
	
	  var defaults = {
	    origin: '*',
	    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
	    preflightContinue: false
	  };
	
	  function isString(s) {
	    return typeof s === 'string' || s instanceof String;
	  }
	
	  function isOriginAllowed(origin, allowedOrigin) {
	    if (Array.isArray(allowedOrigin)) {
	      for (var i = 0; i < allowedOrigin.length; ++i) {
	        if (isOriginAllowed(origin, allowedOrigin[i])) {
	          return true;
	        }
	      }
	      return false;
	    } else if (isString(allowedOrigin)) {
	      return origin === allowedOrigin;
	    } else if (allowedOrigin instanceof RegExp) {
	      return allowedOrigin.test(origin);
	    } else {
	      return !!allowedOrigin;
	    }
	  }
	
	  function configureOrigin(options, req) {
	    var requestOrigin = req.headers.origin,
	        headers = [],
	        isAllowed;
	
	    if (!options.origin || options.origin === '*') {
	      // allow any origin
	      headers.push([{
	        key: 'Access-Control-Allow-Origin',
	        value: '*'
	      }]);
	    } else if (isString(options.origin)) {
	      // fixed origin
	      headers.push([{
	        key: 'Access-Control-Allow-Origin',
	        value: options.origin
	      }]);
	      headers.push([{
	        key: 'Vary',
	        value: 'Origin'
	      }]);
	    } else {
	      isAllowed = isOriginAllowed(requestOrigin, options.origin);
	      // reflect origin
	      headers.push([{
	        key: 'Access-Control-Allow-Origin',
	        value: isAllowed ? requestOrigin : false
	      }]);
	      if (isAllowed) {
	        headers.push([{
	          key: 'Vary',
	          value: 'Origin'
	        }]);
	      }
	    }
	
	    return headers;
	  }
	
	  function configureMethods(options) {
	    var methods = options.methods || defaults.methods;
	    if (methods.join) {
	      methods = options.methods.join(','); // .methods is an array, so turn it into a string
	    }
	    return {
	      key: 'Access-Control-Allow-Methods',
	      value: methods
	    };
	  }
	
	  function configureCredentials(options) {
	    if (options.credentials === true) {
	      return {
	        key: 'Access-Control-Allow-Credentials',
	        value: 'true'
	      };
	    }
	    return null;
	  }
	
	  function configureAllowedHeaders(options, req) {
	    var headers = options.allowedHeaders || options.headers;
	    if (!headers) {
	      headers = req.headers['access-control-request-headers']; // .headers wasn't specified, so reflect the request headers
	    } else if (headers.join) {
	        headers = headers.join(','); // .headers is an array, so turn it into a string
	      }
	    if (headers && headers.length) {
	      return {
	        key: 'Access-Control-Allow-Headers',
	        value: headers
	      };
	    }
	    return null;
	  }
	
	  function configureExposedHeaders(options) {
	    var headers = options.exposedHeaders;
	    if (!headers) {
	      return null;
	    } else if (headers.join) {
	      headers = headers.join(','); // .headers is an array, so turn it into a string
	    }
	    if (headers && headers.length) {
	      return {
	        key: 'Access-Control-Expose-Headers',
	        value: headers
	      };
	    }
	    return null;
	  }
	
	  function configureMaxAge(options) {
	    var maxAge = options.maxAge && options.maxAge.toString();
	    if (maxAge && maxAge.length) {
	      return {
	        key: 'Access-Control-Max-Age',
	        value: maxAge
	      };
	    }
	    return null;
	  }
	
	  function applyHeaders(headers, res) {
	    for (var i = 0, n = headers.length; i < n; i++) {
	      var header = headers[i];
	      if (header) {
	        if (Array.isArray(header)) {
	          applyHeaders(header, res);
	        } else if (header.key === 'Vary' && header.value) {
	          vary(res, header.value);
	        } else if (header.value) {
	          res.setHeader(header.key, header.value);
	        }
	      }
	    }
	  }
	
	  function cors(options, req, res, next) {
	    var headers = [],
	        method = req.method && req.method.toUpperCase && req.method.toUpperCase();
	
	    if (method === 'OPTIONS') {
	      // preflight
	      headers.push(configureOrigin(options, req));
	      headers.push(configureCredentials(options, req));
	      headers.push(configureMethods(options, req));
	      headers.push(configureAllowedHeaders(options, req));
	      headers.push(configureMaxAge(options, req));
	      headers.push(configureExposedHeaders(options, req));
	      applyHeaders(headers, res);
	
	      if (options.preflightContinue) {
	        next();
	      } else {
	        res.statusCode = 204;
	        res.end();
	      }
	    } else {
	      // actual response
	      headers.push(configureOrigin(options, req));
	      headers.push(configureCredentials(options, req));
	      headers.push(configureExposedHeaders(options, req));
	      applyHeaders(headers, res);
	      next();
	    }
	  }
	
	  function middlewareWrapper(o) {
	    // if no options were passed in, use the defaults
	    if (!o) {
	      o = {};
	    }
	    if (o.origin === undefined) {
	      o.origin = defaults.origin;
	    }
	    if (o.methods === undefined) {
	      o.methods = defaults.methods;
	    }
	    if (o.preflightContinue === undefined) {
	      o.preflightContinue = defaults.preflightContinue;
	    }
	
	    // if options are static (either via defaults or custom options passed in), wrap in a function
	    var optionsCallback = null;
	    if (typeof o === 'function') {
	      optionsCallback = o;
	    } else {
	      optionsCallback = function (req, cb) {
	        cb(null, o);
	      };
	    }
	
	    return function (req, res, next) {
	      optionsCallback(req, function (err, options) {
	        if (err) {
	          next(err);
	        } else {
	          var originCallback = null;
	          if (options.origin && typeof options.origin === 'function') {
	            originCallback = options.origin;
	          } else if (options.origin) {
	            originCallback = function (origin, cb) {
	              cb(null, options.origin);
	            };
	          }
	
	          if (originCallback) {
	            originCallback(req.headers.origin, function (err2, origin) {
	              if (err2 || !origin) {
	                next(err2);
	              } else {
	                var corsOptions = Object.create(options);
	                corsOptions.origin = origin;
	                cors(corsOptions, req, res, next);
	              }
	            });
	          } else {
	            next();
	          }
	        }
	      });
	    };
	  }
	
	  // can pass either an options hash, an options delegate, or nothing
	  module.exports = middlewareWrapper;
	})();

/***/ },
/* 50 */
/***/ function(module, exports) {

	/*!
	 * vary
	 * Copyright(c) 2014-2015 Douglas Christopher Wilson
	 * MIT Licensed
	 */
	
	'use strict';
	
	/**
	 * Module exports.
	 */
	
	module.exports = vary;
	module.exports.append = append;
	
	/**
	 * Variables.
	 */
	
	var separators = /[\(\)<>@,;:\\"\/\[\]\?=\{\}\u0020\u0009]/;
	
	/**
	 * Append a field to a vary header.
	 *
	 * @param {String} header
	 * @param {String|Array} field
	 * @return {String}
	 * @api public
	 */
	
	function append(header, field) {
	  if (typeof header !== 'string') {
	    throw new TypeError('header argument is required');
	  }
	
	  if (!field) {
	    throw new TypeError('field argument is required');
	  }
	
	  // get fields array
	  var fields = !Array.isArray(field) ? parse(String(field)) : field;
	
	  // assert on invalid fields
	  for (var i = 0; i < fields.length; i++) {
	    if (separators.test(fields[i])) {
	      throw new TypeError('field argument contains an invalid header');
	    }
	  }
	
	  // existing, unspecified vary
	  if (header === '*') {
	    return header;
	  }
	
	  // enumerate current values
	  var val = header;
	  var vals = parse(header.toLowerCase());
	
	  // unspecified vary
	  if (fields.indexOf('*') !== -1 || vals.indexOf('*') !== -1) {
	    return '*';
	  }
	
	  for (var i = 0; i < fields.length; i++) {
	    var fld = fields[i].toLowerCase();
	
	    // append value (case-preserving)
	    if (vals.indexOf(fld) === -1) {
	      vals.push(fld);
	      val = val ? val + ', ' + fields[i] : fields[i];
	    }
	  }
	
	  return val;
	}
	
	/**
	 * Parse a vary header into an array.
	 *
	 * @param {String} header
	 * @return {Array}
	 * @api private
	 */
	
	function parse(header) {
	  return header.trim().split(/ *, */);
	}
	
	/**
	 * Mark that a request is varied on a header field.
	 *
	 * @param {Object} res
	 * @param {String|Array} field
	 * @api public
	 */
	
	function vary(res, field) {
	  if (!res || !res.getHeader || !res.setHeader) {
	    // quack quack
	    throw new TypeError('res argument is required');
	  }
	
	  // get existing header
	  var val = res.getHeader('Vary') || '';
	  var header = Array.isArray(val) ? val.join(', ') : String(val);
	
	  // set new header
	  if (val = append(header, field)) {
	    res.setHeader('Vary', val);
	  }
	}

/***/ }
/******/ ]);
//# sourceMappingURL=api-core.js.map