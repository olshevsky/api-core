/**
 * Created by igor.olshevsky on 8/12/15.
 */
var Webpack = require('webpack');
var colors = require('colors');
var moment = require('moment');
var config = require('./../webpack.config');

var compiler = Webpack(config);

compiler.watch({
    aggregateTimeout: 300,
    poll: true
}, function(err, stats) {
    if(err) {
        console.error(err)
    } else {
        console.log(colors.green("["+moment().format("H:m:s")+"][Webpack][Core]: Build"));
    }
});