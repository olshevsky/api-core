/**
 * Created by igor.olshevsky on 8/13/15.
 */

export default {
    application: {
        id: "pockemon",
        user: "pockemon",
        project: "api",
        version: 1
    },
    project: {
        data: {
            role: [
                {
                    name: "Owner",
                    alias: 'owner',
                    value: 0
                },
                {
                    name: "Admin",
                    alias: 'admin',
                    value: 1
                },
                {
                    name: "Guest",
                    alias: 'guest',
                    value: Number.MAX_VALUE
                }
            ]
        },
        models: [
            {
                name: "User",
                alias: "user",
                structure: {
                    username: {
                        type: 'string',
                        default: ''
                    },
                    gcmID: {
                        type: 'string',
                        default: ''
                    },
                    oauth: {
                        type: 'array',
                        default: []
                    }
                }
            },
            {
                name: "Role",
                alias: "role",
                structure: {
                    name: {
                        type: 'string',
                        default: ''
                    },
                    value: {
                        type: 'integer',
                        default: 10
                    }
                }
            }
        ]
    },
    system: {
        models: {
            __models: {
                name: "__Models",
                alias: "__models",
                type: 'system',
                structure: {
                    name: {
                        type: 'string',
                        default: ''
                    },
                    alias: {
                        type: 'string',
                        default: ''
                    },
                    type: {
                        type: 'string',
                        default: ''
                    },
                    structure: {
                        type: 'object',
                        default: {}
                    },
                    permissions: {
                        type: 'object',
                        default: {}
                    }
                }
            }
        }
    }
}