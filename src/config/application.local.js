/**
 * Created by igor.olshevsky on 8/13/15.
 */

export default {
    application: {
        id: "adlab_tms_api",
        user: "adlab",
        project: "tms",
        port: 9999
    }
}