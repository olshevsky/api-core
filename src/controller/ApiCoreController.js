/**
 * Created by igor.olshevsky on 8/12/15.
 */
import {Router} from 'express'
import RolesStore from './../store/RolesStore.js'
import modelAction from './../middleware/action.js'
import ModelActionHelper from './../helper/ModelActionHelper.js'
import RestController from './RestController.js'

class ApiCoreController extends RestController {
    constructor(entity, model, metadata = {}) {
        super(entity, model.prebindings, model);

        this.router = Router();
        this.model = model;
        this.metadata = metadata;

        this.url = `/${metadata.user}/${metadata.project}/api/:version/${model.alias}/:id?`;
        this.router.use(this.url, modelAction);
        this.router.use(this.url, this.check.bind(this));

        this.router.get(this.url, this.get.bind(this));
        this.router.post(this.url, this.post.bind(this));
        this.router.put(this.url, this.post.bind(this));
        this.router.patch(this.url, this._patch.bind(this));
        this.router.delete(this.url, this._delete.bind(this));
    }

    check(req, res, next) {
        req.__version = req.params.version;
        let permissions = this.model.permissions || {};
        let actionPermissions = permissions[req.modelAction] || {};
        let userPermissions = actionPermissions[req.userRole] || false;
        if(Array.isArray(userPermissions)) {
            let orQuery = [];
            userPermissions.forEach(field => {
                orQuery.push({[field]: req.user._id});
                orQuery.push({[field]: {
                    $in: [req.user._id]
                }})
            });
            req.queryCliteria = orQuery;
        }

        req.actionPermissions = {
            all: permissions,
            action: actionPermissions,
            user: userPermissions
        };

        if(userPermissions || userPermissions == '*') {
            next();
        } else {
            res.status(403).json({success: false, permissions: req.actionPermissions, message: "Permissions denied."});
        }
    }

    get(req, res, next) {
        let id = req.params.id;

        if(id) {
            this.single(req.params.id, req, res, next)
        } else {
            this.list(req, res, next)
        }
    }

    post(req, res, next) {
        let data = req.body,
            id = req.params.id;
        if(!id) {
            this.create(data, req, res, next)
        } else {
            this.update(id, data, req, res, next)
        }
    }

    _patch(req, res, next) {
        let data = req.body,
            id = req.params.id;
        if(id) {
            this.patch(id, data, req, res, next)
        } else {
            res.json({success: false})
        }
    }

    _delete(req, res, next) {
        this.delete(req.params.id, req, res, next)
    }
}

export default ApiCoreController