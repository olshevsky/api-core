/**
 * Created by igor.olshevsky on 8/13/15.
 */
import {Router} from 'express'
import passport from 'passport'
import LocalStrategy from 'passport-local'
import User from './../document/User.js'
import RestController from './RestController.js'
import TokenService from './../service/TokenService.js'

class AuthController extends RestController {
    constructor(entity, model, metadata) {
        passport.use(new LocalStrategy(entity.authenticate()));
        passport.serializeUser(entity.serializeUser());
        passport.deserializeUser(entity.deserializeUser());

        super(entity, model.prebindings);

        this.entity = entity;
        this.model = model;

        this.metadata = metadata;
        this.urls = {

            auth: `/${metadata.user}/${metadata.project}/auth/api/:version/${model.alias}`,
            rest: `/${metadata.user}/${metadata.project}/api/:version/${model.alias}/:id?`
        };

        this.router = Router();
        //this.router.use(this.urls.auth, this.check.bind(this));
        this.router.use(this.urls.rest, this.check.bind(this));

        this.router.get(this.urls.auth, this.user.bind(this));
        this.router.post(this.urls.auth, this.authenticate.bind(this));

        this.router.get(this.urls.rest, this.get.bind(this));
        this.router.post(this.urls.rest, this.post.bind(this));
        this.router.put(this.urls.rest, this.post.bind(this));
        this.router.patch(this.urls.rest, this._patch.bind(this));
        this.router.delete(this.urls.rest, this._delete.bind(this));
    }

    check(req, res, next) {
        req.__version = req.params.version;
        let permissions = this.model.permissions || {};
        let actionPermissions = permissions[req.modelAction] || {};
        let userPermissions = actionPermissions[req.userRole] || false;

        if(Array.isArray(userPermissions)) {
            let orQuery = [];
            userPermissions.forEach(field => {
                orQuery.push({[field]: req.user._id})
            });
            req.queryCliteria = orQuery;
        }

        req.actionPermissions = {
            all: permissions,
            action: actionPermissions,
            user: userPermissions
        };

        if(userPermissions || userPermissions == '*') {
            next();
        } else {
            res.status(403).json({success: false, message: "Permissions denied."});
        }
    }

    get(req, res, next) {
        let id = req.params.id;
        if(id) {
            if(id == 'list') {
                this.list(req.params.id, req, res, next)
            } else {
                this.single(req.params.id, req, res, next)
            }
        } else {
            res.json(req.user);
        }
    }

    post(req, res, next) {
        let data = req.body,
            id = req.params.id;
        if(!id) {
            this.create(data, req, res, next)
        } else {
            this.update(id, data, req, res, next)
        }
    }

    _patch(req, res, next) {
        let data = req.body,
            id = req.params.id;
        if(id) {
            this.patch(id, data, req, res, next)
        } else {
            res.json({success: false})
        }
    }

    _delete(req, res, next) {
        this.delete(req.params.id, req, res, next)
    }

    authenticate(req, res, next) {
        (passport.authenticate('local', {session: false}, function(err, user, info) {
            if(user) {
                TokenService.getToken(user).then((token) => {
                    res.json({user, token});
                })
            } else {
                res.json({err, info, user});
            }
        }))(req, res, next)
    }

    user(req, res, next) {
        res.json(req.user)
    }
}

export default AuthController