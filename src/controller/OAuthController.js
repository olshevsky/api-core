/**
 * Created by igor.olshevsky on 8/13/15.
 */
import {Router} from 'express'
import User from './../document/User.js'
import RestController from './RestController.js'
import passport from 'passport'
import LocalStrategy from 'passport-local'

class AuthController extends RestController {
    constructor(entity, model, metadata) {
        super(entity, '');
        this.metadata = metadata;
        this.urls = {
            oauth: `/${metadata.user}/${metadata.project}/oauth/:provider`,
            callback: `/${metadata.user}/${metadata.project}/oauth/:provider/callback`
        };
        this.router = Router();
        this.router.use(this.urls.oauth, this.check.bind(this));
        this.router.get(this.urls.oauth, this.oauth.bind(this));

        this.router.get(this.urls.callback, this.callback.bind(this));
    }

    check(req, res, next) {
        req.__version = req.params.version;
        next()
    }

    oauth(req, res, next) {
        (passport.authenticate(req.params.provider))(req, res, next);
    }

    callback(req, res, next) {
        (passport.authenticate(req.params.provider, {
            session: false,
            failureRedirect: '/auth'
        }, function(err, user, info) {
            if(user) {
                res.json({user, info});
            } else {
                res.json({err, info, user});
            }
        }))(req, res, next);
    }
}

export default AuthController