/**
 * Created by igor.olshevsky on 8/12/15.
 */
import Controller from './Controller.js'
import {Assert, Validator} from 'validator.js'
import ValidationFactory from './../helper/ValidationFactory.js'

class RestController extends Controller {
    constructor(entity, prebinding, schema = {}) {
        super();

        this.entity = entity;
        this.schema = schema;
        this.validator = ValidationFactory.getValidation(schema.structure);
        this.prebinding = prebinding || '';
    }

    single(id, req, res, next) {
        let query = this.entity.findOne({_id: id});
        if(req.queryCliteria) {
            query.or(req.queryCliteria);
        }

        query.populate(this.prebinding).exec((err, model) => {
            if(err) {
                res.json(err);
            } else {
                if(model && model != null) {
                    res.json({model});
                } else {
                    res.status(404).json({success: false, message: "Not Found"});
                }
            }
        });
    }

    list(req, res, next) {
        let query = this.entity.find({});

        if(req.queryCliteria) {
            query.or(req.queryCliteria);
        }

        query.populate(this.prebinding).exec((err, model) => {
            if(err) {
                res.json(err);
            } else {
                if(model && model != null) {
                    res.json(model);
                } else {
                    res.status(404).json({success: false, message: "Not Found"});
                }
            }
        });
    }

    create(data, req, res, next) {
        let validated = ValidationFactory.validate(data, this.validator);
        if(validated.valid) {
            if(req.token) {data.creator = req.user._id}
            let object = new (this.entity)(data);
            object.save((err) => {
                if(err) {
                    res.json(err)
                } else {
                    res.json(object)
                }
            })
        } else {
            res.json({success: false, error: validated})
        }
    }

    update(id, data, req, res, next) {
        let filter = ValidationFactory.validate(data, this.validator);
        if (filter.valid) {
            let query = this.entity.findOne({_id: id});
            if(req.queryCliteria) {
                query.or(req.queryCliteria);
            }
            query.populate(this.prebinding).exec((err, model) => {
                if(err) {
                    res.json(err);
                } else {
                    if(model && model != null) {
                        Object.keys(filter.map).forEach(key => {
                            model[key] = data[key]
                        });
                        model._id = model._id.toString();
                        this.entity.update(model, (err) => {
                            if(err) {
                                res.json(err)
                            } else {
                                res.json(model)
                            }
                        });
                    } else {
                        res.status(404).json({success: false, message: "Not Found"});
                    }
                }
            });
        } else {
            res.json({success: false, filter})
        }
    }

    patch(id, data, req, res, next) {
        let filter = ValidationFactory.setValidationGroup(data, Object.keys(data), this.validator);

        if (filter.valid) {
            let query = this.entity.findOne({_id: id});
            if(req.queryCliteria) {
                query.or(req.queryCliteria);
            }
            query.populate(this.prebinding).exec((err, model) => {
                if(err) {
                    res.json(err);
                } else {
                    if(model && model != null) {
                        Object.keys(filter.map).forEach(key => {
                            model[key] = req.body[key]
                        });
                        model._id = model._id.toString();
                        this.entity.update(model, (err) => {
                            if(err) {
                                res.json(err)
                            } else {
                                res.json(model)
                            }
                        });
                    } else {
                        res.status(404).json({success: false, message: "Not Found"});
                    }
                }
            });
        } else {
            res.json({success: false, error: filter})
        }
    }

    delete(id, req, res, next) {
        let query = this.entity.findOne(id);
        if(req.queryCliteria) {
            query.or(req.queryCliteria);
        }
        query.remove().exec((err) => {
            if(err) {
                res.json({success: false})
            } else {
                res.json({_id: id, success: true})
            }
        });
    }
}

export default RestController