import mongoose from 'mongoose'
import timestamps from 'mongoose-timestamp'
import config from './../config'
import CManager from './../service/ConnectionManager.js'

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var Model = new Schema({
    name: String,
    alias: String,
    permissions: {},
    structure: {},
    meta: {},
    type: String
}, {collection: "__models"});

Model.plugin(timestamps);

export default CManager.application.model('Model', Model);
export const TYPES = {
    USER_ENTITY: "USER_ENTITY",
    API_ENTITY: "API_ENTITY"
};
export function make(connection) {
    return connection.model('Model', Model)
}