import mongoose from 'mongoose'
import timestamps from 'mongoose-timestamp'
import config from './../config'
import CManager from './../service/ConnectionManager.js'

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var Project = new Schema({
    link: String,
    authentication: {
        model: {type: ObjectId, ref: 'Model'},
        login_route: String,
        credentials: {
            identity: String,
            credential: String
        }
    }
}, {collection: "__project"});

Project.plugin(timestamps);

export default CManager.application.model('Project', Project);

export function make(connection) {
    return connection.model('Model', Project)
}