import mongoose from 'mongoose'
import timestamps from 'mongoose-timestamp'
import config from './../config'
import CManager from './../service/ConnectionManager.js'

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var Role = new Schema({
    name: String,
    alias: String,
    value: Number
});

Role.plugin(timestamps);

export default CManager.application.model('Role', Role);

export function make(connection) {
    return connection.model('Role', Role)
}