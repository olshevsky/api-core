import mongoose from 'mongoose'
import timestamps from 'mongoose-timestamp'
import config from './../config'
import CManager from './../service/ConnectionManager.js'

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var Token = new Schema({
    token: String,
    metadata: {},
    user: {type: ObjectId, ref: 'User'},
    expires: Date
});

Token.plugin(timestamps);

export default CManager.application.model('Token', Token);

export function make(connection) {
    return connection.model('Token', Token)
}