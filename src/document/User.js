import mongoose from 'mongoose'
import timestamps from 'mongoose-timestamp'
import passportLocalMongoose from 'passport-local-mongoose'
import config from './../config'
import CManager from './../service/ConnectionManager.js'

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var User = new Schema({
    gcmID: {type: String},
    role: {type: String},
    oauth: [{
        provider: {type: String},
        key: {type: String}
    }]
}, {collection: "users"});

User.plugin(timestamps);
User.plugin(passportLocalMongoose, {
    usernameUnique: true
});

User.methods.toJSON = function() {
    var obj = this.toObject();
    delete obj.salt;
    delete obj.hash;
    delete obj.password;
    return obj
};

export default CManager.application.model('User', User);
export function extend(schema) {
    return (sch, options) => {
        Object.keys(schema).forEach(p => {
            sch.add({[p]: schema[p]})
        })
    }
}
export var UserSchema = User;
export function make(connection) {
    return connection.model('User', User)
}