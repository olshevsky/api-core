/**
 * Created by igor.olshevsky on 9/6/15.
 */
export default {
    CREATE: 'create',

    READ: 'read',
    READ_SINGLE: 'read_single',
    READ_LIST: 'read_list',

    UPDATE: 'update',
    DELETE: 'delete'
}