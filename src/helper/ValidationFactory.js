/**
 * Created by igor.olshevsky on 8/28/15.
 */
import Validator from 'validator'
class ValidationFactory {
    static getValidation(structure) {
        let validatorsHash = {};
        if(structure) {
            (Object.keys(structure)).map(property => {
                let propertyValidators = [];
                let description = structure[property];
                if(description.hasOwnProperty('validators') && Array.isArray(description.validators)) {
                    propertyValidators = description.validators.map(validator => {
                        return {
                            type: validator.type,
                            validate: ValidationFactory.convertValidator(validator)
                        }
                    });
                }
                validatorsHash[property] = propertyValidators;
            });
        }

        return validatorsHash;
    }

    static validate(data, validator) {
        let validationResult = {}
            ,kv = {}
            ,resultsArray = [];

        Object.keys(validator).map(key => {
            let propertyValid = true;
            let propertyValidationResult = {};
            validator[key].map(v => {
                propertyValid = v.validate(data[key]);
                propertyValidationResult[v.type] = propertyValid;
            });
            kv[key] = propertyValid;
            resultsArray.push(propertyValid);
            validationResult[key] = propertyValidationResult;
        });

        return {
            valid: !resultsArray.filter(r => !r).length,
            map: kv,
            properties: validationResult
        };
    }


    static carry(validator, ...args) {
        return (value) => { return validator(value, args) }
    };

    static setValidationGroup(data, group, validator) {
        Object.keys(validator).forEach((field) => {
            if(group.indexOf(field) == -1) {
                delete validator[field]
            };
        });
        return ValidationFactory.validate(data, validator);
    }

    static convertValidator(desc) {
        let {type, metadata} = desc;
        metadata = metadata || {};
        switch(type) {
            case ValidationFactory.TYPES.CONTAINS: {
                return ValidationFactory.carry(Validator.contains, metadata.seed);
            }
            case ValidationFactory.TYPES.AFTER: {
                return;
            }
            case ValidationFactory.TYPES.ALPHA: {
                return;
            }
            case ValidationFactory.TYPES.ALPHANUMERIC: {
                return;
            }
            case ValidationFactory.TYPES.ASCII: {
                return;
            }
            case ValidationFactory.TYPES.BASE64: {
                return;
            }
            case ValidationFactory.TYPES.BEFORE: {
                return;
            }
            case ValidationFactory.TYPES.BOOLEAN: {
                return;
            }
            case ValidationFactory.TYPES.BYTE_LENGTH: {
                return;
            }
            case ValidationFactory.TYPES.CREDIT_CARD: {
                return;
            }
            case ValidationFactory.TYPES.CURRENCY: {
                return;
            }
            case ValidationFactory.TYPES.DATE: {
                return;
            }
            case ValidationFactory.TYPES.DECIMAL: {
                return;
            }
            case ValidationFactory.TYPES.DIVISIBLE_BY: {
                return;
            }
            case ValidationFactory.TYPES.EMAIL: {
                return;
            }
            case ValidationFactory.TYPES.FQDN: {
                return;
            }
            case ValidationFactory.TYPES.FLOAT: {
                return;
            }
            case ValidationFactory.TYPES.FULLWIDTH: {
                return;
            }
            case ValidationFactory.TYPES.HALFWIDTH: {
                return;
            }
            case ValidationFactory.TYPES.HEX_COLOR: {
                return;
            }
            case ValidationFactory.TYPES.HEXADECIMAL: {
                return;
            }
            case ValidationFactory.TYPES.IP: {
                return;
            }
            case ValidationFactory.TYPES.ISBN: {
                return;
            }
            case ValidationFactory.TYPES.ISO8601: {
                return;
            }
            case ValidationFactory.TYPES.IN: {
                return;
            }
            case ValidationFactory.TYPES.JSON: {
                return;
            }
            case ValidationFactory.TYPES.LENGTH: {
                return;
            }
            case ValidationFactory.TYPES.LOWERCASE: {
                return;
            }
            case ValidationFactory.TYPES.MONGO_ID: {
                return;
            }
            case ValidationFactory.TYPES.MULTIBYTE: {
                return;
            }
            case ValidationFactory.TYPES.NULL: {
                return;
            }
            case ValidationFactory.TYPES.NUMERIC: {
                return ValidationFactory.carry(Validator.isNumeric);
            }
            case ValidationFactory.TYPES.URL: {
                return;
            }
            case ValidationFactory.TYPES.UUID: {
                return;
            }
            case ValidationFactory.TYPES.UPPERCASE: {
                return;
            }
            case ValidationFactory.TYPES.VARIABLE_WIDTH: {
                return;
            }
            case ValidationFactory.TYPES.MATCHES: {
                return;
            }
            case ValidationFactory.TYPES.INT: {
                return;
            }
            case ValidationFactory.TYPES.EQUALS: {
                return;
            }
            default:
                return;
        }
    }
}

ValidationFactory.TYPES = {
    CONTAINS: 'contains',
    AFTER: 'after',
    ALPHA: 'alpha',
    ALPHANUMERIC: 'alphanumeric',
    ASCII: 'ascii',
    BASE64: 'base64',
    BEFORE: 'before',
    BOOLEAN: 'boolean',
    BYTE_LENGTH: 'byteLength',
    CREDIT_CARD: 'creditCard',
    CURRENCY: 'currency',
    DATE: 'date',
    DECIMAL: 'decimal',
    DIVISIBLE_BY: 'divisibleBy',
    EMAIL: 'email',
    FQDN: 'qbdn',
    FLOAT: 'float',
    FULLWIDTH: 'fullwidth',
    HALFWIDTH: 'halfwidth',
    HEX_COLOR: 'hexColor',
    HEXADECIMAL: 'hexadecimal',
    IP: 'ip',
    ISBN: 'isbn',
    ISO8601: 'iso8601',
    IN: 'in',
    JSON: 'json',
    LENGTH: 'length',
    LOWERCASE: 'lowercase',
    MONGO_ID: 'mongoId',
    MULTIBYTE: 'mongoId',
    NULL: 'null',
    NUMERIC: 'numeric',
    URL: 'url',
    UUID: 'uuid',
    UPPERCASE: 'uppercase',
    VARIABLE_WIDTH: 'variableWidth',
    MATCHES: 'matches',
    INT: 'int',
    EQUALS: 'equals'
};

export default ValidationFactory