/**
 * Created by igor.olshevsky on 9/9/15.
 */

import mongoose from 'mongoose'
import timestamps from 'mongoose-timestamp'

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;


export default (schema, opt) => {
    schema.plugin(timestamps);
    schema.add({
        creator: {ref: "User", type: ObjectId},
        __status: {type: Boolean, default: true}
    });
}