/**
 * Created by igor.olshevsky on 8/13/15.
 */

import express from 'express'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import passport from 'passport'
import response from './middleware/response.js'
import not_found from './middleware/not-found.js'
import CManager from './service/ConnectionManager.js'
import UrlHelper from './helper/UrlHelper.js'
import {Server} from 'http'
import SocketIO from 'socket.io'
import server_error from './middleware/server-error.js'
import config from './config'
import RStore from './store/RolesStore.js'
import TokenService from './service/TokenService.js'
import EntityManager from './service/EntityManager.js'
import Bootstrap from './service/Bootstrap.js'

let app = express();
let server = Server(app);
let io = SocketIO(server);

app.use(response);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('cors')());
app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());

let project = process.env.project || config.application.project,
    user = process.env.user || config.application.user,
    port = process.env.port || config.application.port,
    prefix = `/${user}/${project}`
    ;

io.of(prefix).on('connection', (socket) => {

});

Bootstrap.init({project, user, port}, app).then((router) => {
    app.use(router);
    Bootstrap.systemApi({project, user, port});
    app.use(not_found);

    app.use(server_error);

    console.log(JSON.stringify({success: true, prefix, message: `Application ${project} for ${user} started`, metadata: process.env, models: Object.keys(EntityManager.hash)}));
    server.listen(port || 3000);
});