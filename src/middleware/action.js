/**
 * Created by igor.olshevsky on 9/9/15.
 */

import ModelActionHelper from './../helper/ModelActionHelper.js'
export default (req, res, next) => {
    req.__version = req.params.version;
    let id = req.params.id;
    switch(req.method.toLocaleLowerCase()) {
        case 'get' : {
            req.modelAction = id ? ModelActionHelper.READ_SINGLE : ModelActionHelper.READ_LIST;
            break;
        }
        case 'post' : {
            req.modelAction = id ? ModelActionHelper.UPDATE : ModelActionHelper.CREATE;
            break;
        }
        case 'patch' : {
            req.modelAction = id ? ModelActionHelper.UPDATE : null;
            break;
        }
        case 'delete' : {
            req.modelAction = ModelActionHelper.DELETE;
            break;
        }
    }

    next();
};