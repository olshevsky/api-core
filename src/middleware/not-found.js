/**
 * Created by igor.olshevsky on 8/13/15.
 */

import ErrorResponse from './../response/ErrorResponse.js'

export default (req, res, next) => {
    var err = new Error('Not Found');
    err.status = 404;
    res.response(new ErrorResponse("Not Found", {}, false, 404));
}