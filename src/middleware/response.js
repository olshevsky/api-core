/**
 * Created by igor.olshevsky on 8/13/15.
 */

export default function(req, res, next) {
    res.response = response => {
        response.response = res;
        response.send();
    };

    next();
}