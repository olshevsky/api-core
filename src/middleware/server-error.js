/**
 * Created by igor.olshevsky on 8/13/15.
 */

import ErrorResponse from './../response/ErrorResponse.js'

export default (err, req, res, next) => {
    console.log(err);
    res.response(new ErrorResponse(err.message, {
        stack: err.stack
    }, false, 500));
}