import ErrorResponse from './../response/ErrorResponse.js'

export default (Token) => {
    return (
        (req, res, next) => {
            let token = req.headers.token;
            let app = req.headers.app;
            if(!token) {
                req.token = false;
                req.userRole = 'guest';
                req.user = {
                    name: "Guest",
                    type: 'guest',
                    role: 'guest'
                };
                next()
            } else {
                Token.findOne({token}).populate('user').exec((error, token) => {
                    if(token && token != null) {
                        req.token = token;
                        req.user = token.user;
                        req.userRole = req.user.role;
                        next()
                    } else {
                        res.status(403).json({success: false, message: `Token for is not found`});
                    }
                })
            }
        }
    )
}