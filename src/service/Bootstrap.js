/**
 * Created by igor.olshevsky on 8/13/15.
 */

import {make as makeModel, TYPES as ModelTypes} from './../document/Model.js'
import {make as makeProject} from './../document/Project.js'
import {Router} from 'express'
import Project from './../document/Project.js'
import config from './../config'
import Generator from './Generator.js'
import userMiddleware from './../middleware/user.js'
import ApiCoreController from './../controller/ApiCoreController.js'
import AuthController from './../controller/AuthController.js'
import OAuthController from './../controller/OAuthController.js'
import CManager from './../service/ConnectionManager.js'
import UrlHelper from './../helper/UrlHelper.js'
import Promise from 'promise'
import EntityManager from './EntityManager.js'
import Role from './../document/Role.js'
import RolesStore from './../store/RolesStore.js'
import {make as makeToken} from './../document/Token.js'

class Bootstrap {
    static api(env) {
        return Bootstrap.bootstrapResources(env)
    }

    static systemApi(env) {
        let connection = CManager.get(UrlHelper.build([env.user, env.project]));
        let models = config.system.models || {};
        let RestRouter = {};
        Object.keys(models).forEach(key => {
            let model = models[key];
            let entity = Generator.createSchema(connection, model);
            if(model.type == 'system') {
                RestRouter = new ApiCoreController(entity, model, env);
            }
        });
        return RestRouter.router || Router()
    }

    static bootstrapResources(env) {
        let connection = CManager.get(UrlHelper.build([env.user, env.project]));
        let Model = makeModel(connection);
        let TokenModel = makeToken(connection);
        EntityManager.add(TokenModel, "Token");
        EntityManager.add(Model, "Model");

        let router = Router();
        return Promise.all([
            new Promise((resolve, reject) => {

                router.use('*', userMiddleware(TokenModel));

                Model.find({}).exec((err, models) => {
                    if(err) {
                        reject(err)
                    } else {
                        models.forEach(model => {
                            let entity = Generator.createSchema(connection, model);
                            switch(model.type) {
                            case ModelTypes.USER_ENTITY:
                                    let Auth = new AuthController(entity, model, env);
                                    let OAuth = new OAuthController(entity, model, env);

                                    router.use(Auth.router);
                                    router.use(OAuth.router);
                                    break;
                                default:
                                    let RestRouter = new ApiCoreController(entity, model, env);
                                    router.use(RestRouter.router);
                            }
                        })
                    }
                    resolve(router);
                })
            }),
            new Promise((resolve, reject) => {
                Role.find({}).exec((err, models) => {
                    models.forEach((model) => RolesStore.add(model));
                    resolve(true)
                })
            })
        ]).then((results) => {
            return results[0];
        });
    }

    static init(env) {
        return Bootstrap.api(env);
    }
}

export default Bootstrap