/**
 * Created by igor.olshevsky on 8/13/15.
 */

import mongoose from 'mongoose'
import config from './../config'
import UrlHelper from './../helper/UrlHelper.js'

class ConnectionManager {
    constructor() {
        this.connections = {};
    }

    get application() {
        return this.get(UrlHelper.build([config.application.user, config.application.project]));
    }

    createConnection(args) {
        return this.get(UrlHelper.build(args));
    }

    get(url) {
        if(!this.connections.hasOwnProperty(url)) {
            this.connections[url] = mongoose.createConnection(ConnectionManager.getUrl(url))
        }

        return this.connections[url]
    }

    static getUrl(collection, credentials = false) {
        if(!credentials) {
            return `mongodb://localhost/${collection}`
        } else {
            return `mongodb://localhost/${collection}`
        }
    }
}

export default new ConnectionManager()