class EntityManager {
    constructor() {
        this.hash = {}
    }

    add(entity, name = false) {
        this.hash[name || entity.constructor.name] = entity;
    }

    has(name) {
        return !!this.get(name)
    }

    get(name) {
        return this.hash[name]
    }
}

export default new EntityManager()