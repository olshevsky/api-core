/**
 * Created by igor.olshevsky on 8/13/15.
 */
import TypeChecker from './TypeChecker.js'
import {TYPES} from './../document/Model.js'
import passport from 'passport'
import mongoose from 'mongoose'
import abstractEntity from './../helper/abstractEntity.js'
import EntityManager from './EntityManager.js'
import {make, UserSchema, extend as extendUser} from './../document/User.js'
import LocalStrategy from 'passport-local'

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

class Generator {
    static createSchema(connection, model) {
        let {name, structure, alias} = model;
        let schema = {};
        if(structure) {

            Object.keys(structure).forEach(property => {
                let description = structure[property];
                schema[property] = TypeChecker.convert(description.type, description)
            });
            let MongooseModel = {};
            switch(model.type) {
                case TYPES.USER_ENTITY : {
                    UserSchema.plugin(extendUser(schema));
                    MongooseModel = connection.model(alias, UserSchema);
                    break;
                }
                default: {
                    let Model = new Schema(schema);
                    Model.plugin(abstractEntity);
                    MongooseModel = connection.model(alias, Model);
                    break;
                }
            }
            EntityManager.add(MongooseModel, model.name);
            return MongooseModel;
        } else {
           throw "Schema not found!"
        }
    }
}

export default Generator