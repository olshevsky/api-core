import randomstring from 'randomstring'
import Promise from 'promise'
import EM from './EntityManager.js'

class TokenService {
    getToken(user) {
        let Token;
        if(EM.has("Token")) {
            Token  = EM.get("Token")
        } else {
            throw new Error("Token Entity does not found")
        }

        return new Promise((resolve, reject) => {
            Token.findOne({user: user._id}).exec((error, token) => {
                if(!error) {
                    if(!token) {
                        this.createToken(user).then(token => {
                            resolve(token)
                        }, error => reject(error))
                    } else {
                        resolve(token)
                    }
                } else {
                    reject(error)
                }
            });
        });
    }
    createToken(user) {
        let Token;
        if(EM.has("Token")) {
            Token  = EM.get("Token")
        } else {
            throw new Error("Token Entity does not found")
        }

        return new Promise(function(resolve, reject) {
            let token = new Token({
                token: randomstring.generate(32),
                user
            });

            token.save((err, token) => {
                if(!err) {
                    resolve(token)
                } else {
                    reject(err)
                }
            })
        })
    }
}

export default new TokenService();