/**
 * Created by igor.olshevsky on 9/7/15.
 */
import config from './../config'

class RolesStore {
    constructor() {
        this.roles = config.project.data.role
    }

    add(role) {
        this.roles.push(role)
    }

}

export default new RolesStore()