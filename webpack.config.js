/**
 * Created by igor.olshevsky on 8/12/15.
 */
var webpack = require('webpack');
var path = require('path');
var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
    .filter(function(x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function(mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });

module.exports = {
    name: 'server',
    target: 'node',
    entry: './src/index.js',
    devtool: 'sourcemap',
    output: {
        path: './bin',
        filename: 'api-core.js'
    },
    node: {
        globals: true,
        __filename: true,
        __dirname: true
    },
    root: [ path.join(__dirname, 'src'), path.join(__dirname, 'node_modules') ],
    externals: nodeModules,
    module: {
        loaders: [
            { test: /\.js$/, loader: 'babel-loader', exclude: ['./node_modules'] }
        ]
    },
    plugins: [
        new webpack.BannerPlugin('require("source-map-support").install();',
            { raw: true, entryOnly: false })
    ],
    resolve: {
        root:  path.resolve(__dirname, 'src')
    }
};